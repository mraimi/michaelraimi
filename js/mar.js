$(document).ready(function buttonShowPic(oldButton, newButton, picture)
  {
    $(oldButton).click(function(){
	    $(picture).fadeIn();
	    $(oldButton).hide();
	    $(newButton).show();
	});
});

$(document).ready(function buttonHidePic(oldButton, newButton, picture)
  {
    $(oldButton).click(function(){
      $(picture).fadeOut();
      $(oldButton).hide()
      $(newButton).show();
    });
});
